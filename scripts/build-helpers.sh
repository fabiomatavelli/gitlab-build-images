function image_already_exists() {
    image=$1

    $(docker pull $image > /dev/null)
}

function build_if_needed() {
    image=$1

    if ! image_already_exists $image; then
        docker build -t "$image" -f "Dockerfile.$image" .
    else
        echo "$image already exists, skipping build."
    fi
}

function push_if_needed() {
    image=$1

    if ! image_already_exists $image; then
        docker push "$image"
    else
        echo "$image already exists, skipping push."
    fi
}
